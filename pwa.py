from flask import Flask, render_template, request, redirect, url_for, flash, jsonify
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config['SECRET_KEY'] = 'super-secret'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


# ####################
# MODELS
# ####################

class Note(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.Unicode)

    def __repr__(self):
        return '<Note %r>' % self.id


db.create_all()


# ####################
# MODEL_VIEWS
# ####################


class NoteMV(object):
    @staticmethod
    def get_all():
        return Note.query.all()

    @staticmethod
    def create(body, **kwargs):
        if not body:
            raise Exception("No body in note creation.")
        note = Note(body=body)
        db.session.add(note)
        db.session.commit()
        return note

    @staticmethod
    def delete(note_id, **kwargs):
        note = Note.query.filter_by(id=note_id).first()
        if not note:
            raise Exception("No note with id {}".format(note_id))
        db.session.delete(note)
        db.session.commit()


# ####################
# VIEWS
# ####################


@app.route('/')
def home():
    notes = NoteMV.get_all()
    return render_template('index.html',
                           notes=notes)


@app.route('/notes/<int:note_id>/delete')
def notes_delete(note_id):
    j = bool(request.json)
    try:
        NoteMV.delete(note_id)
    except Exception as e:
        if j:
            return jsonify({
                "Error": str(e)
            }), 400
        else:
            flash(str(e))
    return redirect(url_for('home'))


@app.route('/notes/create', methods=["POST"])
def notes_create():
    j = False
    if request.form:
        body = request.form.get('body')
    else:
        j = True
        body = request.json.get('body')
    try:
        note = NoteMV.create(body=body)
    except Exception as e:
        if j:
            return jsonify({
                "Error": str(e)
            }), 400
        else:
            flash(str(e))
    if j:
        return jsonify({
            'id': note.id,
            'body': note.body,
            '_html': render_template('note.html', note=note)
        })
    return redirect(url_for('home'))


if __name__ == '__main__':
    app.run(debug=True)
